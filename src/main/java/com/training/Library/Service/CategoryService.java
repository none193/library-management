package com.training.Library.Service;

import com.training.Library.Model.Category;
import com.training.Library.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public List<Category> getAllCategory(){
        List<Category> categories = new ArrayList<Category>();
        categoryRepository.findAll().forEach(category -> categories.add(category));
        return  categories;
    }

    public Category getCategoryById(int id){
        return categoryRepository.findById(id).get();
    }

    public void saveOrUpdate(Category category){
        categoryRepository.save(category);
    }

    public void delete(int id){
        categoryRepository.deleteById(id);
    }
}
