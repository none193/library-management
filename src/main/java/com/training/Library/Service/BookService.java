package com.training.Library.Service;

import com.training.Library.Model.Book;
import com.training.Library.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    public List<Book> getAllBooks(){
        List<Book> books = new ArrayList<Book>();
        bookRepository.findAll().forEach(book -> books.add(book));
        return books;
    }

    public List<Book> getBooksByCategory(int id){
        List<Book> books = new ArrayList<Book>();
        bookRepository.findByCategoryId(id).forEach(book -> books.add(book));
        return books;
    }

    public List<Book> getBooksByUser(int id){
        List<Book> books = new ArrayList<Book>();
        bookRepository.findByUserId(id).forEach(book -> books.add(book));
        return books;
    }

    public Book getBookById(int id){
        return bookRepository.findById(id).get();
    }

    public  void saveOrUpdate(Book book){
        bookRepository.save(book);
    }

    public  void delete(int id){
        bookRepository.deleteById(id);
    }
}
