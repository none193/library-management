package com.training.Library.Controller;

import com.training.Library.Model.Category;
import com.training.Library.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("/categories")
    private List<Category> getAllCategory(){
        return categoryService.getAllCategory();
    }

    @GetMapping("/category/{id}")
    private Category getCategory(@PathVariable("id") int id){
        return  categoryService.getCategoryById(id);
    }

    @DeleteMapping("/category/{id}")
    private void deleteCategory(@PathVariable("id") int id){
        categoryService.delete(id);
    }

    @PostMapping("/category")
    private  int saveCategory(@RequestBody Category category){
        categoryService.saveOrUpdate(category);
        return category.getId();
    }
}
