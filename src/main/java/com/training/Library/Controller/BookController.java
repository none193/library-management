package com.training.Library.Controller;

import com.training.Library.Model.Book;
import com.training.Library.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping("/books")
    private List<Book> getAllBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping("category/{id}/books")
    private List<Book> getBooksByCategory(@PathVariable("id") int id){
        return bookService.getBooksByCategory(id);
    }

    @GetMapping("user/{id}/books")
    private List<Book> getBooksByUser(@PathVariable("id") int id){
        return bookService.getBooksByUser(id);
    }

    @GetMapping("/books/{id}")
    private Book getBookById(@PathVariable("id") int id){
        return bookService.getBookById(id);
    }

    @PostMapping("/book")
    private int saveBook(@RequestBody Book book){
        bookService.saveOrUpdate(book);
        return book.getId();
    }

    @DeleteMapping("/book/{id}")
    private void deleteBook(@PathVariable("id") int id){
        bookService.delete(id);
    }
}
