package com.training.Library.Repository;

import com.training.Library.Model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {

    public List<Book> findByCategoryId(int id);
    public List<Book> findByUserId(int id);
}
